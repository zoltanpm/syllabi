\documentclass[11pt, oneside]{article}


% Packages
\usepackage{titling}
\pretitle{\begin{flushright}\large}
\posttitle{\par\end{flushright}\vskip 0.5em}
\preauthor{\begin{flushleft} \lineskip 0.5em}
\postauthor{\par\end{flushleft}}
\date{}

\usepackage{setspace}
\usepackage[margin=1in]{geometry}
\usepackage{mdwlist}
\setlength{\parindent}{0cm}


% Front Matter
\def\myauthor{Zoltan P. Majdik, Ph.D.}
\def\course{\textbf{COMM 711: Communication Theory}}
\def\address{Minard 338C2}
\def\contact{NDSU email/skype: zoltan.majdik@ndsu.edu}
\def\phone{701–231–8038}
\def\hours{Tuesdays from 9:30 am---11:30 am, or by appointment}
\def\coursenum{\#7697 }
\def\credits{3 }
\def\sem{Spring 2017}
\def\meettime{T 7:30---10:00 pm }
\def\location{Minard 410}

\title{\course \\ \footnotesize \hfill Course \coursenum --- \credits Credits --- \sem \\ \meettime --- \location}

\author{\footnotesize instructor: \hfill \myauthor \\ office: \hfill \address
\\ contact: \hfill \contact \\ phone: \hfill \phone \\ office hours: \hfill \hours}



\begin{document}

\maketitle




\section*{Bulletin Description and Objectives}

This course provides graduate students with a structured forum for theorizing
communication. Our objectives are:


\begin{itemize}
\item to develop a knowledge base about what theory is, what theories consist of, how theories are built, and how theories can be evaluated.

\item to learn about the different historical and contemporary traditions of inquiry, schools of theorizing, and debates over theory that have shaped the communication discipline.

\item to develop the skill to use theory---in discussion and in writing---for addressing intellectual and/or practical problems related to communication.
\end{itemize}




\section*{Course Materials}

\begin{itemize*}
\item Craig, Robert T., and Heidi L. Muller. Theorizing Communication:
    Readings Across Traditions. Thousand Oaks, CA: Sage Publications, 2007.
\item Additional readings, distributed in class or via Blackboard
\end{itemize*}


\newpage


\section*{Course Requirements and Grading}

\begin{itemize*}
\item Ph.D. paper \dotfill 60\%
\item 2 Masters papers, each \dotfill 30\%
\item Theory-in-use pr\'ecis \dotfill 20\%
\item Presentation \dotfill 10\%
\item Discussion \dotfill 10\%
\end{itemize*}


\subsubsection*{\hfill Grading Philosophy}

A work reflects interesting and unique work. A work requires you to go beyond the basic requirements of an assignment, to demonstrate insightful, original thinking, and to present your thinking in a well-researched, well-argued, carefully supported, structured, and stylistically compelling way.
\\ \\
B work reflects a thorough understanding of the material discussed, as well as some original insight on your part. B work contains a clear thesis, and it requires you to demonstrate that you can apply what you read, study, and research to make an insightful argument. B work is turned in on time, and free of major grammatical and typographical errors.
\\ \\
C work reflects a basic understanding of the material discussed and meets the basic requirements of the assignment. It responds to the questions asked, but adds little that is unique or interesting on its own. C work offers a thesis and makes an argument, but often fails to adequately support that argument through reasoning and research. C works often contains too many errors, is not well structured, and/or is stylistically deficient.
\\ \\
D work reflects a lack of basic understanding of the course material, and it does not respond adequately to the questions asked. D work presents opinions without the research and evidence needed to make good arguments. D work contains more than average grammatical and/or typographical errors.
\\ \\
F work fails to respond to the questions asked by the assignment. F work reflects very little or no understanding of the material discussed, it does not meet the basic requirements of the assignment, and/or is presented in a way that fails to meet basic conventions of grammar, style, or typography.


\subsubsection*{\hfill Grade Ranges}

90\% $\le$ A $\le$ 100\% \\
80\% $\le$ B $<$ 90\% \\
70\% $\le$ C $<$ 80\% \\
60\% $\le$ D $<$ 70\% \\
F $<$ 60\%


\newpage


\section*{How to Succeed in this Course}

\textit{Discussion} --- Participation in discussion is an integral part of graduate seminars, both for your own benefit as well as for the benefit of your peers. The best way to get an A in this course is to do the readings prior to class in a way that allows you to understand class discussions and lectures, allows you to participate, and allows you to ask questions. Regular, active participation---whether answering questions, contributing ideas, or asking questions---is what counts for your discussion grade.
\\ \\
\textit{Talk to Me} --- If you have any questions, issues, or concerns about
the course material, or just want to talk about ideas from readings and
discussions, come talk to me in my office. If you want to make sure I'm in my
office, or can't make it in because it's just too cold, send me a Skype request
and we can discuss your questions that way. I'm only logged into Skype when I'm
at work, so don't hesitate to contact me that way when you see me listed as
available.
\\ \\
\textit{Weekly Questions} --- For each reading, please come
prepared with at least one question you'd like the class to discuss. This
question can be about the reading itself, about connections between readings,
or about how our readings help illuminate some aspect of the communication
problem you're working on in your paper(s). The questions should be deep and interesting enough to warrant prolonged examination and discussion (i.e., not "what are the two elements of a sign in Saussure's semiotics?"). We will discuss some of your questions at the beginning or during our discussion.
\\ \\
\textit{Attendance} --- Attendance is expected for every class, and necessary to understand both the ideas we discuss every week and how these ideas connect to each other. 
\\ \\
\textit{Deadlines} --- Late assignments will receive a deduction of one letter gradation (e.g., from B to B-) for every day they are turned in late, weekends included. Extensions can only be given if you had a documented family or medical emergency (please let me know of any such emergency as soon as you can).
\\ \\
\textit{Grade Complaints} --- Grade challenges must be made within 10 days after receiving a grade. Grade challenges must be made in writing, and present a reasoned justification for a new grade. A granted grade challenge results in a full regrading of the assignment; your revised grade may be higher, the same, or lower as your original grade. The revised grade is final.
\\ \\
\textit{Academic Integrity} --- The academic community is operated on the basis of honesty, integrity, and fair play. NDSU Policy 335: Code of Academic Responsibility and Conduct applies to cases in which cheating, plagiarism, or other academic misconduct have occurred in an instructional context. Students found guilty of academic misconduct are subject to penalties, up to and possibly including suspension and/or expulsion. Student academic misconduct records are maintained by the Office of Registration and Records. Informational resources about academic honesty for students and instructional staff members can be found at www.ndsu.edu/academichonesty. Please familiarize yourselves with NDSU's policies on academic misconduct; ignorance of these policies cannot be used in defense of academic misconduct.
\\ \\
\textit{Students with Disabilities} --- Any students with disabilities or other
special needs, who need special accommodations in this course are invited to
share these concerns or requests with the instructor and contact the Disability
Services Office as soon as possible.


\newpage


% Course Schedule

\section*{Course Schedule}



% week 1
\subsubsection*{\hfill Week 1 --- Introduction to Communication Theory}

\begin{description*}
\item January 10

Craig and Muller, Introduction (in-class reading)
\end{description*}



% week 2
\subsubsection*{\hfill Week 2 --- Historical and Cultural Sources of Communication Theory}

\begin{description*}
\item January 17

Craig and Muller, Unit I (pp. 1-53)
\end{description*}


% week 3
\subsubsection*{\hfill Week 3 --- Creating and Critiquing Theory I: Vocabularies}

\begin{description*}
\item January 24

Reynolds, A Primer in Theory Construction (excerpt)

Kuhn, Objectivity, Value Judgment, and Theory Choice

Miller, chapters 2-4 (via Blackboard)
\end{description*}



% week 4
\subsubsection*{\hfill Week 4 --- Creating and Critiquing Theory II: Theorizing Communication}

\begin{description*}
\item January 31

Weick, Theory Construction as Disciplined Imagination

Craig and Muller, Unit II (pp. 55-101)

Miller, chapter 1 (optional)

Rindova, Publishing Theory When You Are New to the Game (Ph.D. students only)
\end{description*}



% week 5
\subsubsection*{\hfill Week 5 --- Rhetorical Theory I: Classical Beginnings}

\begin{description*}
\item February 7

Gorgias, Encomium of Helen

Craig and Muller, Unit III (pp. 103-130; choice between excerpts and full texts)

Plato, "Phaedrus"
\end{description*}



% week 6
\subsubsection*{\hfill Week 6 --- Rhetorical Theory II: Contemporary Directions}

\begin{description*}
\item February 14

Burke, Rhetoric Old and New

Scott, On Viewing Rhetoric as Epistemic

Hariman, Status, Marginality, and Rhetorical Theory

Charland, Constitutive Rhetoric
\end{description*}



% week 7
\subsubsection*{\hfill Week 7 --- The Semiotic Tradition}

\begin{description*}
\item February 21

Craig and Muller, Unit IV (pp. 163-215)

\end{description*}



% week 8
\subsubsection*{\hfill Week 8 --- The Sociopsychological Tradition I: Foundations}

\begin{description*}
\item February 28

Craig and Muller, Unit VII (pp. 313-363)
\end{description*}



% week 9
\subsubsection*{\hfill Week 9 --- The Sociopsychological Tradition II: Different Directions}

\begin{description*}
\item March 7

Delia, O'Keefe, and O'Keefe, The Constructivist Approach to Communication

Borman, Symbolic Convergence Theory and Communication in Group Decision-Making

Greene, A Cognitive Approach to Human Communication:  An Action Assembly Theory
\end{description*}


\begin{description*}
\item March 10

    \textbf{Masters paper 1 \& Ph.D. early drafts due}
\end{description*}

% week 10
\subsubsection*{\hfill Week 10 --- Spring Break}



% week 11
\subsubsection*{\hfill Week 11 --- The Cybernetic Tradition}

\begin{description*}
\item March 21

Craig and Muller, Unit VI (pp. 261-313)
\end{description*}



% week 12
\subsubsection*{\hfill Week 12 --- The Sociocultural Tradition I: Precursors}

\begin{description*}
\item March 28

Plato, Allegory of the Cave

Kant, What is Enlightenment

Giddens, The Contours of High Modernity (chapter)

Geertz, Thick Description: Toward an Interpretive Theory of Culture
\end{description*}



% week 13
\subsubsection*{\hfill Week 13 --- The Sociocultural Tradition II: In COMM}

\begin{description*}
\item April 4

Craig and Muller, Unit VIII (pp. 365-423)
\end{description*}



% week 14
\subsubsection*{\hfill Week 14 --- The Cultural/Critical Tradition}

\begin{description*}
\item April 11

Craig and Muller, Unit IX (pp. 425-493)

\end{description*}


\begin{description*}
\item April 13

\textbf{Theory-in-use pr\'ecis due}
\end{description*}



% week 15
\subsubsection*{\hfill Week 15 --- The Phenomenological Tradition}

\begin{description*}
\item April 18

Craig and Muller, Unit V (pp. 217-259)

\end{description*}



% week 16
\subsubsection*{\hfill Week 16 --- Peer Review Day}

\begin{description*}
\item April 22

Peer review of paper drafts --- please bring written draft to class   
\end{description*}



% week 17
\subsubsection*{\hfill Week 17 --- Presentations}

\begin{description*}
\item May 2

Powers, On the Intellectual Structure of the Human Communication Discipline (optional)
\end{description*}



%post-term
\subsubsection*{\hfill Final Assignments}

\begin{description*}
\item May 10

\textbf{Masters paper 2 and Ph.D. paper due}
\end{description*}



\end{document}

\documentclass[11pt, oneside]{article}


% Packages
\usepackage{titling}
\pretitle{\begin{flushright}\large}
\posttitle{\par\end{flushright}\vskip 0.5em}
\preauthor{\begin{flushleft} \lineskip 0.5em}
\postauthor{\par\end{flushleft}}
\date{}

\usepackage{setspace}
\usepackage[margin=1in]{geometry}
\usepackage{mdwlist}
\setlength{\parindent}{0cm}


% Front Matter
\def\prof{Zoltan P. Majdik, Ph.D.}
\def\course{\textbf{COMM 767: Rhetorical Criticism}}
\def\address{Minard 338C2}
\def\email{zoltan.majdik@ndsu.edu}
\def\phone{701–231–8038}
\def\officehours{Tuesdays from 10 am---noon, or by appointment}
\def\coursenum{\#17794 }
\def\credits{3 }
\def\semester{Fall 2016}
\def\meettime{Th 5:00---7:30 pm }
\def\location{COMM Conference Room}

\title{\course \\ \footnotesize \hfill Course \coursenum --- \credits Credits
--- \semester \\ \meettime --- \location}

\author{\footnotesize instructor: \hfill \prof \\ office: \hfill \address
\\ email: \hfill \email \\ phone: \hfill \phone \\ office hours: \hfill \officehours}



\begin{document}

\maketitle


\section*{Bulletin Description and Objectives}

This course is about understanding and analyzing how we use
    language in civic affairs.
It examines key moments, texts, and debates in rhetorical criticism from the
20th and 21st century. Through these texts, we will assemble, evaluate, and
apply a range of critical methods of inquiry that may be applied to oral and
written discourse. Our objectives are

\begin{itemize}
\item to gain insight into the field of rhetorical criticism and its
    historical and theoretical developments in the 20th and early 21st
    century.
\item to gain a diverse methodological understanding of the means of inquiry available to
the rhetorical critic for selecting, analyzing, and evaluating texts.
\item to learn how to conceptualize and write a single-authored paper,
    using advanced theoretical and methodological understandings of rhetorical
    criticism, that can be submitted to either a journal or a national
conference.
\end{itemize}




\section*{Course Materials}

\begin{itemize*}
\item Burgchardt, Carl R., ed. Readings in Rhetorical Criticism. 4th
ed. State College, PA: Strata Pub, 2010.
\item Additional readings, distributed in class or via Blackboard
\end{itemize*}


\newpage


\section*{Course Requirements and Grading}

\begin{itemize*}
\item Final paper \dotfill 40\%
\item Pr\'ecis, each \dotfill 15\%
\item Presentation \dotfill 20\%
\item Discussion \dotfill 10\%
\end{itemize*}


\subsubsection*{\hfill Grading Philosophy}

A work reflects interesting and unique work. A work requires you to go beyond the basic requirements of an assignment, to demonstrate insightful, original thinking, and to present your thinking in a well-researched, well-argued, carefully supported, structured, and stylistically compelling way.
\\ \\
B work reflects a thorough understanding of the material discussed, as well as some original insight on your part. B work contains a clear thesis, and it requires you to demonstrate that you can apply what you read, study, and research to make an insightful argument. B work is turned in on time, and free of major grammatical and typographical errors.
\\ \\
C work reflects a basic understanding of the material discussed and meets the basic requirements of the assignment. It responds to the questions asked, but adds little that is unique or interesting on its own. C work offers a thesis and makes an argument, but often fails to adequately support that argument through reasoning and research. C works often contains too many errors, is not well structured, and/or is stylistically deficient.
\\ \\
D work reflects a lack of basic understanding of the course material, and it does not respond adequately to the questions asked. D work presents opinions without the research and evidence needed to make good arguments. D work contains more than average grammatical and/or typographical errors.
\\ \\
F work fails to respond to the questions asked by the assignment. F work reflects very little or no understanding of the material discussed, it does not meet the basic requirements of the assignment, and/or is presented in a way that fails to meet basic conventions of grammar, style, or typography.


\subsubsection*{\hfill Grade Ranges}

90\% $\le$ A $\le$ 100\% \\
80\% $\le$ B $<$ 90\% \\
70\% $\le$ C $<$ 80\% \\
60\% $\le$ D $<$ 70\% \\
F $<$ 60\%


\newpage


\section*{How to Succeed in this Course}

\textit{Discussion} --- Participation in discussion is an integral part of graduate seminars, both for your own benefit as well as for the benefit of your peers. The best way to get an A in this course is to do the readings prior to class in a way that allows you to understand class discussions and lectures, allows you to participate, and allows you to ask questions. Regular, active participation---whether answering questions, contributing ideas, or asking questions---is what counts for your discussion grade.
\\ \\
\textit{Talk to Me} --- If you have any questions, issues, or concerns about
the course material, or just want to talk about ideas from readings and
discussions, come talk to me in my office. I'm available during office hours,
but you can email me to set up a meeting at other times as well. 
\\ \\
\textit{Weekly Questions} --- For each reading, please come
prepared with at least one question you'd like the class to discuss. This
question can be about the reading itself, about connections between readings,
or about how our readings help illuminate some aspect of the communication
problem you're working on in your paper(s). The questions should be deep and interesting enough to warrant prolonged examination and discussion (i.e., not "what are the two elements of a sign in Saussure's semiotics?"). We will discuss some of your questions at the beginning or during our discussion.
\\ \\
\textit{Attendance} --- Attendance is expected for every class, and necessary to understand both the ideas we discuss every week and how these ideas connect to each other. 
\\ \\
\textit{Deadlines} --- Late assignments will receive a deduction of one letter gradation (e.g., from B to B-) for every day they are turned in late, weekends included. Extensions can only be given if you had a documented family or medical emergency (please let me know of any such emergency as soon as you can).
\\ \\
\textit{Grade Complaints} --- Grade challenges must be made within 10 days after receiving a grade. Grade challenges must be made in writing, and present a reasoned justification for a new grade. A granted grade challenge results in a full regrading of the assignment; your revised grade may be higher, the same, or lower as your original grade. The revised grade is final.
\\ \\
\textit{Academic Integrity} --- The academic community is operated on the basis of honesty, integrity, and fair play. NDSU Policy 335: Code of Academic Responsibility and Conduct applies to cases in which cheating, plagiarism, or other academic misconduct have occurred in an instructional context. Students found guilty of academic misconduct are subject to penalties, up to and possibly including suspension and/or expulsion. Student academic misconduct records are maintained by the Office of Registration and Records. Informational resources about academic honesty for students and instructional staff members can be found at www.ndsu.edu/academichonesty. 
\\ \\
\textit{Students with Disabilities} --- Any students with disabilities or other
special needs, who need special accommodations in this course are invited to
share these concerns or requests with the instructor and contact the Disability
Services Office as soon as possible.


\newpage


% Course Schedule

\section*{Course Schedule}



% week 1
\subsubsection*{\hfill Week 1 --- What is Rhetoric? How do we do Rhetorical
Analyses?}

\begin{description*}
\item August 25

In-class readings
\end{description*}



% week 2
\subsubsection*{\hfill Week 2 --- Primer in the History of Rhetoric}

\begin{description*}
\item September 1

Readings by Gorgias, Plato, Aristotle
\end{description*}


% week 3
\subsubsection*{\hfill Week 3 --- Historical Debates on the Scope of Rhetorical Analysis}

\begin{description*}
\item September 8

Readings by Wichelns, Black, Wrage, Wander
\end{description*}



% week 4
\subsubsection*{\hfill Week 4 --- Neo-Aristotelian and Seminal Analyses}

\begin{description*}
\item September 15

    Readings by Leff \& Mohrmann, Browne, Hill, and Leff ("Dimensions of
    Temporality")
\end{description*}



% week 5
\subsubsection*{\hfill Week 5 --- Beyond Single Texts: Social Movements}

\begin{description*}
\item September 22

Readings by Griffin, Simons, Zaeske, Lake 
\end{description*}



% week 6
\subsubsection*{\hfill Week 6 --- Narratives and Dramas}

\begin{description*}
\item September 29

Readings by Fisher, Burke, Ling, Tonn et al.
\end{description*}



% week 7
\subsubsection*{\hfill Week 7 --- Close Focus on Language: Metaphors,
Metonymies, and More}

\begin{description*}
\item October 6

Readings by Vico, Lucas, Ivie, ?
\end{description*}



% week 8
\subsubsection*{\hfill Week 8 --- Genres and Themes}

\begin{description*}
\item October 13

Readings by Campbell and Jamieson, Ware \& Linkugel, Darsey, Sullivan
\end{description*}



% week 9
\subsubsection*{\hfill Week 9 --- Ideology}

\begin{description*}
\item October 20

Readings by McGee, Lucaites \& Condit, Black, ?
\end{description*}



% week 10
% \subsubsection*{\hfill Week 10 --- Spring Break}



% week 10
\subsubsection*{\hfill Week 10 --- Directions in Critical Rhetoric}

\begin{description*}
\item October 27

    Readings by McKerrow, Nakayama \& Krizek, Cloud, Campbell, Dow, Morris III,
    Sloop, etc., by groups
\end{description*}



% week 11
\subsubsection*{\hfill Week 11 --- State of the Art: Debates}

\begin{description*}
\item November 3

Readings on Big Rhetoric/little rhetoric and audience in rhetoric, by groups
\end{description*}



% week 12
\subsubsection*{\hfill Week 12 --- NCA}

%\begin{description*}
%\end{description*}



% week 13
\subsubsection*{\hfill Week 13 --- State of the Art: Effects}

\begin{description*}
\item November 17

Readings TBD
\end{description*}



% week 14
\subsubsection*{\hfill Week 14 --- Thanksgiving}

%\begin{description*}
%\end{description*}



% week 15
\subsubsection*{\hfill Week 15 --- State of the Art: Topic Areas}

\begin{description*}
\item December 1

Readings on science rhetoric, presidential/political rhetoric, and visual
rhetoric,
by groups
\end{description*}



% week 16
\subsubsection*{\hfill Week 16 --- Presentations}

\begin{description*}
\item December 8

Short readings on academic presentations
\end{description*}



%post-term
\subsubsection*{\hfill Final Assignments}

\begin{description*}
\item December 12

    \textbf{Papers due}
\end{description*}



\end{document}
